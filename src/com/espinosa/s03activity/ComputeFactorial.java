package com.espinosa.s03activity;

import java.math.BigInteger;
import java.text.DecimalFormat;
import java.util.Scanner;

public class ComputeFactorial {
    public static void main(String[] args) throws Exception {
        try{
            Scanner in = new Scanner(System.in);
            int num;
            BigInteger answer = new BigInteger("1");
            BigInteger counter = new BigInteger("1");

            System.out.println("LET'S COMPUTE THE FACTORIAL OF A NUMBER :)");
            System.out.println("Enter a number: ");
            num = in.nextInt();
            in.close();

            // Guard Clause
            if (num < 0) throw new Exception("Number should be positive");

            // Computing the factorial
            while (counter.intValue() <= num) {
                answer = answer.multiply(counter);
                counter = counter.add(new BigInteger("1"));
            }

            System.out.println("The factorial of " + num + " is " + new DecimalFormat("###,###,###").format(answer));

        } catch (Exception e) {
            System.out.println(e.getMessage() == null ? "Invalid input" : e.getMessage());
        }
    }



}
